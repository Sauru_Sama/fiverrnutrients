$(document).ready(function() {

    //-----------------------------------
    var myobject_bloom = {
        9: '9',
        10: '10',
        11: '11',
        12: '12',
        13: '13',
        14: '14',
        15: '15',
        16: '16'
    };

    var select_bloom = document.getElementById("id_week_bloom");
    for (index in myobject_bloom) {
        select_bloom.options[select_bloom.options.length] = new Option(myobject_bloom[index], index);
    }
    //-----------------------------------
    var myobject_veg = {
        1: '1',
        2: '2',
        3: '3',
        5: '5',
        6: '6',
        7: '7'
    };

    var select_veg = document.getElementById("id_week_veg");
    for (index in myobject_veg) {
        select_veg.options[select_veg.options.length] = new Option(myobject_veg[index], index);
    }

    //---------------------------------------------------------     

    var veg_default = $('#id_week_veg').find(":selected").val();
    var bloom_default = $('#id_week_bloom').find(":selected").val();
    var grower_level_default = $('#id_growing-level').find(":selected").val();
    showHideWeeks_bloom(grower_level_default, veg_default, bloom_default);
    defaultStart(grower_level_default);

    //---------------------------------------------------------   

}); //ready

//---------------------------------------------------------

function defaultStart(grower_level) {
    var v_reservoir = Number($('#reservoir').val());

    switch (grower_level) {
        case 'basic':
            rowValues(v_reservoir, 0, 'ROOTS');
            rowValues(v_reservoir, 1, 'PRE');
            rowValues(v_reservoir, 2, 'ULTRA');
            rowValues(v_reservoir, 3, 'PHAT');
            rowValues(v_reservoir, 4, 'OOZE');
            rowValues(v_reservoir, 5, 'FINAL');

            rowValues(v_reservoir, 7, 'GROW_A');
            rowValues(v_reservoir, 8, 'GROW_B');
            rowValues(v_reservoir, 9, 'FLOWER_A');
            rowValues(v_reservoir, 10, 'FLOWER_B');

            rowValues(v_reservoir, 12, 'EURO_A');
            rowValues(v_reservoir, 13, 'EURO_B');
            break;

        case 'intermediate':
            rowValues(v_reservoir, 0, 'ROOTS');
            rowValues(v_reservoir, 1, 'PHAT');
            rowValues(v_reservoir, 2, 'OOZE');
            rowValues(v_reservoir, 3, 'CAL_MAG');
            rowValues(v_reservoir, 4, 'ORGSWTNR');
            rowValues(v_reservoir, 5, 'SEA_FUEL');

            rowValues(v_reservoir, 7, 'GROW_A');
            rowValues(v_reservoir, 8, 'GROW_B');
            rowValues(v_reservoir, 9, 'FLOWER_A');
            rowValues(v_reservoir, 10, 'FLOWER_B');

            rowValues(v_reservoir, 12, 'EURO_A');
            rowValues(v_reservoir, 13, 'EURO_B');
            break;

        case 'deluxe':
            rowValues(v_reservoir, 0, 'ROOTS');
            rowValues(v_reservoir, 1, 'PRE');
            rowValues(v_reservoir, 2, 'ULTRA');
            rowValues(v_reservoir, 3, 'PHAT');
            rowValues(v_reservoir, 4, 'OOZE');
            rowValues(v_reservoir, 5, 'FINAL');
            rowValues(v_reservoir, 6, 'SEA_MINERALS');
            rowValues(v_reservoir, 7, 'CAL_MAG');
            rowValues(v_reservoir, 8, 'ORGSWTNR');
            rowValues(v_reservoir, 9, 'PK');
            rowValues(v_reservoir, 10, 'SILICA');
            rowValues(v_reservoir, 11, 'SEA_FUEL');
            rowValues(v_reservoir, 12, 'SEA_WEED');

            rowValues(v_reservoir, 14, 'GROW_A');
            rowValues(v_reservoir, 15, 'GROW_B');
            rowValues(v_reservoir, 16, 'FLOWER_A');
            rowValues(v_reservoir, 17, 'FLOWER_B');

            rowValues(v_reservoir, 19, 'EURO_A');
            rowValues(v_reservoir, 20, 'EURO_B');
            break;

        default:
            break;
    }

    $('.transparent-input').attr('disabled', 'disabled');
}

//---------------------------------------------------------   

function mlValue(row, veg, bloom) {
    var table = $('#dataTable').DataTable();
    var v_cells = table.columns(':visible').count();
    var v_ml = 0;

    for (let i = 0; i < v_cells; i++) {
        var table_cell = table.cell(row, i).nodes().to$().find('center').html();

        if (table_cell != null && table_cell != '') {
            v_ml = v_ml + Number(table_cell);
        }
    }

    var total = row_total(row, veg, bloom, Number(v_ml));

    return round(Number(total), 2);
}

//---------------------------------------------------------   

function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

//---------------------------------------------------------   

function add_ml(row) {
    var table = $('#dataTable').DataTable();

    var celda_others = table.columns(':visible').count();
    for (var index = 1; index < celda_others; index++) {
        var initial_value = table.cell(row, index).nodes().to$().find('center').html();
        if (initial_value == '' || initial_value == null) {
            initial_value == '';
        } else {
            var final_value = initial_value + ' ml';
            table.cell(row, index).nodes().to$().find('center').html(final_value);
        }
    }
}

//---------------------------------------------------------   

function rowValues(reservoir, row, name) {
    var table = $('#dataTable').DataTable();

    var veg = $('#id_week_veg').find(":selected").val();
    var bloom = $('#id_week_bloom').find(":selected").val();

    switch (name) {
        case 'ROOTS':
            if (veg > 2 || bloom >= 6) {
                var higher_week_value = Math.round(2 * reservoir);
                roots_row_values(row, veg, bloom, higher_week_value);
            }

            var value = Math.round(4 * reservoir);
            table.cell(row, 1).nodes().to$().html('<center>' + value + '</center>');
            table.cell(row, 2).nodes().to$().html('<center>' + value + '</center>');

            if (veg == 2) {
                table.cell(row, 3).nodes().to$().find('td').html('<center>' + value + '</center>');
            }

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'PRE':
            var value = Math.round(4 * reservoir);
            pre_row_values(row, veg, value)

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'ULTRA':
            var value = Math.round(4 * reservoir);
            ultra_row_values(row, veg, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';

            if (veg < 5) {
                table.cell(row, (Number(veg) + 2)).nodes().to$().html('<center><span>*&nbsp;</span>' + value + '</center>');
            }

            if (veg >= 5) {
                table.cell(row, 5).nodes().to$().html('<center><span>*&nbsp;</span>' + value + '</center>');
            }

            add_ml(row);
            break;

        case 'PHAT':
            var value = Math.round(10 * reservoir);
            phat_row_values(row, veg, bloom, value)

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'OOZE':
            var value = Math.round(10 * reservoir);
            ooze_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'FINAL':
            var value = Math.round(4 * reservoir);
            final_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'SEA_MINERALS':
            var value = round(0.5 * reservoir, 2);
            sea_minerals_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'CAL_MAG':
            var value = Math.round(4 * reservoir);
            cal_mag_row_values(row, veg, bloom, value)

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'ORGSWTNR':
            var value = Math.round(4 * reservoir);
            orgswtnr_row_values(row, veg, bloom, value)

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'PK':
            var value = Math.round(4 * reservoir);
            pk_row_values(row, veg, bloom, value)

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'SILICA':
            var value = round(1.3 * reservoir, 2);
            silica_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'SEA_FUEL':
            var value = Math.round(4 * reservoir);
            sea_fuel_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'SEA_WEED':
            var value = Math.round(4 * reservoir);
            sea_weed_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

            //---------------

        case 'GROW_A':
            var value = Math.round(8 * reservoir);

            if (veg == 1) {
                table.cell(row, 2).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
            }

            if (veg >= 2) {
                table.cell(row, 2).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
                table.cell(row, 3).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
            }

            if (veg >= 3) {
                table.cell(row, 2).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
                grow_a_row_values(row, veg, bloom, value);
            }

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'GROW_B':
            var value = Math.round(8 * reservoir);

            if (veg == 1) {
                table.cell(row, 2).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
            }

            if (veg >= 2) {
                table.cell(row, 2).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
                table.cell(row, 3).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
            }

            if (veg >= 3) {
                table.cell(row, 2).nodes().to$().html('<center>' + Math.round(4 * reservoir) + '</center>');
                grow_b_row_values(row, veg, bloom, value);
            }

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'FLOWER_A':
            // CLONE
            var clone_value = Math.round(4 * reservoir);

            table.cell(row, 1).nodes().to$().html('<center>' + clone_value + '</center>');

            var value = Math.round(8 * reservoir);
            flower_a_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'FLOWER_B':
            // CLONE
            var clone_value = Math.round(4 * reservoir);

            table.cell(row, 1).nodes().to$().html('<center>' + clone_value + '</center>')

            var value = Math.round(8 * reservoir);
            flower_b_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

            //---------------

        case 'EURO_A':
            var value = Math.round(8 * reservoir);

            if (veg == 1) {
                table.cell(row, 1).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 2).nodes().to$().html('<center>' + value + '</center>');
            }

            if (veg == 2) {
                table.cell(row, 1).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 2).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 3).nodes().to$().html('<center>' + value + '</center>');
            }

            if (veg >= 3) {
                table.cell(row, 1).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 2).nodes().to$().html('<center>' + value + '</center>');
            }

            euro_a_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;

        case 'EURO_B':
            var value = Math.round(8 * reservoir);

            if (veg == 1) {
                table.cell(row, 1).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 2).nodes().to$().html('<center>' + value + '</center>');
            }

            if (veg == 2) {
                table.cell(row, 1).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 2).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 3).nodes().to$().html('<center>' + value + '</center>');
            }

            if (veg >= 3) {
                table.cell(row, 1).nodes().to$().html('<center>' + value + '</center>');
                table.cell(row, 2).nodes().to$().html('<center>' + value + '</center>');
            }

            euro_b_row_values(row, veg, bloom, value);

            // ML
            var v_ml = mlValue(row, veg, bloom) + ' ml';
            add_ml(row);
            break;
    }

    var celda_ml = table.columns(':visible').count() - 1;
    table.cell(row, celda_ml).nodes().to$().html('<center>' + v_ml + '</center>'); //ML	
}

//---------------------------------------------------------

//--show/hide weeks for bloom and veg
$('#id_week_bloom').on('change', function(e) {
    e.preventDefault();

    var veg = $('#id_week_veg').find(":selected").val();
    var bloom = $('#id_week_bloom').find(":selected").val();
    var grower_level = $('#id_growing-level').find(":selected").val();
    showHideWeeks_bloom(grower_level, veg, bloom);
});

$('#id_week_veg').on('change', function(e) {
    e.preventDefault();

    var veg = $('#id_week_veg').find(":selected").val();
    var bloom = $('#id_week_bloom').find(":selected").val();
    var grower_level = $('#id_growing-level').find(":selected").val();
    showHideWeeks_bloom(grower_level, veg, bloom);
});

$('#id_growing-level').on('change', function(e) {
    e.preventDefault();

    var veg = $('#id_week_veg').find(":selected").val();
    var bloom = $('#id_week_bloom').find(":selected").val();
    var grower_level = $('#id_growing-level').find(":selected").val();
    showHideWeeks_bloom(grower_level, veg, bloom);
});
//--show/hide weeks for bloom and veg

function ColumnCount() {
    var numCols = $("#dataTable").find('tbody tr')[0].cells.length;
    $('tfoot tr th').attr('colspan', numCols.toString());
    $('.autoHeader').attr('colspan', numCols.toString());
}

//---------------------------------------------------------   

function showHideWeeks_bloom(grower_level, veg, bloom) {
    $.ajax({
        type: 'GET',
        url: "weeks/" + grower_level + "/veg-" + veg + "-bloom-" + bloom + ".html",
        async: true,
        success: function(data) {
            $("#tabla").children().remove();
            $("#tabla").html(data);
            $('#dataTable').dataTable({
                destroy: true,
                ordering: false,
                searching: false,
                info: false,
                lengthChange: false,
                paging: false,
                autoWidth: false,
                scrollCollapse: false,
                scrollX: false,
                dom: 'Bfrtip',
                buttons: [
                    'print'
                ],
                "initComplete": function() {
                    $('.dataTables_scrollBody thead tr').css({ visibility: 'collapse' });
                }
            });
            $($.fn.dataTable.tables(false)).DataTable().columns.adjust().draw();

            ColumnCount();
            defaultStart(grower_level);

            var reservoir = $('#reservoir').val().trim();
            var gallons = 'gallons';
            if (Number(reservoir) <= 1) {
                gallons = 'gallon';
            }

            $('.reservoirSizeTitle').html(`Amounts needed base on Reservoir size: ${reservoir} ${gallons}`);
        }, //success
        error: function(jqXhr, textStatus, errorThrown) {
                console.log(textStatus + " = " + errorThrown);
            } //error
    }); //ajax
};

//--show/hide weeks for bloom

//---------------------------------------------------------

//--change-reservoir --value
$('#reservoir').on('change keyup', function() {
    var veg = $('#id_week_veg').find(":selected").val();
    var bloom = $('#id_week_bloom').find(":selected").val();
    var grower_level = $('#id_growing-level').find(":selected").val();
    showHideWeeks_bloom(grower_level, veg, bloom);
});
//--change-reservoir --value

//--select-title --nutrient
function tableTitle() {
    var titleTable = '<b>' + $('#id_nutrient').find(":selected").text() + '</b>';

    $('#table-nutrient').html(titleTable);
}

tableTitle();
//--select-title --nutrient

//--select-title --growing-level
function selectGrowingLevelTitle() {
    var title = $('#id_growing-level').find(":selected").text();

    $('#growing-level-title').html(title);
    //tableTitle();
}
//--select-title --growing-level

//--select-title --nutrient
function selectNutrientTitle() {
    var title = $('#id_nutrient').find(":selected").text();

    $('#nutrient-title').html(title);
    tableTitle();
}
//--select-title --nutrient

//--select-title --week_veg
function selectWeekTitle_veg() {
    var title = $('#id_week_veg').find(":selected").text();

    $('#week-title_veg').html(title);
}
selectWeekTitle_veg();
//--select-title --week_veg

//--select-title --week_bloom
function selectWeekTitle() {
    var title = $('#id_week_bloom').find(":selected").text();

    $('#week-title_bloom').html(title);
    tableTitle();
}
selectWeekTitle();
//--select-title --week_bloom

//--print-to-pdf
$("#print_pdf").on('click', function() {
    $("#print_pdf").addClass("running");

    var title = $('#id_nutrient').find(":selected").text().trim();

    // Because of security restrictions, getImageFromUrl will
    // not load images from other domains.  Chrome has added
    // security restrictions that prevent it from loading images
    // when running local files.  Run with: chromium --allow-file-access-from-files --allow-file-access
    // to temporarily get around this issue.
    var getImageFromUrl = function(url, callback) {
        var img = new Image();

        img.onError = function() {
            alert('Cannot load image: "' + url + '"');
        };
        img.onload = function() {
            callback(img);
        };
        img.src = url;
    }

    // Since images are loaded asyncronously, we must wait to create
    // the pdf until we actually have the image.
    // If we already had the JPEG image binary data loaded into
    // a string, we create the pdf without delay.
    var createPDF = function(imgData) {
        html2canvas(document.querySelector("#tabla")).then(canvas => {
            var imagData = canvas.toDataURL('image/jpeg', 1.0);

            L = {
                orientation: 'l',
                unit: 'mm',
                format: 'letter',
                compress: true,
                autoSize: false
            };

            var doc = new jsPDF(L);
            // This is a modified addImage example which requires jsPDF 1.0+
            // You can check the former one at examples/js/basic.js

            //Header
            doc.addImage(imgData, 'PNG', 130, 3, 30, 14, 'logo', 'FAST'); // Cache the image using the alias 'logo'

            doc.setFontSize(12);
            doc.text(25, 18, title);

            doc.setFontSize(8);
            doc.text(25, 22, `*Ultra application for week one of the flower cycle is most effective when applied on its own the last night of veg, preceded by 36 hours of darkness before \n transitioning the lights to a flowering light schedule (12/12)`);
            // As you can guess, using the cached image reduces the generated PDF size by 50%!
            //Header 

            //Table
            doc.addImage(imagData, 'JPEG', 25, 26, 228, 182, 'tabla');
            //Table

            //Footer
            doc.line(20, 210, 260, 210); // horizontal line
            doc.setFontSize(10);
            doc.text(60, 214, 'BloomYellowBottles.US | info@bloomyellowbottles.com | 1 - 84 - 420 - BLOOM(1 - 844 - 202 - 5666)');
            //Footer

            // Output as Data URI
            $("#print_pdf").removeClass("running");
            doc.output('save', 'bloom');
        });
    }

    getImageFromUrl('assets/images/bloom_logo.png', createPDF);
});
//--print-to-pdf