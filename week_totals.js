function replaceWithZero(variable) {
    if (typeof variable == 'undefined' || variable === undefined || variable == '' || variable == null || variable == '0') {
        variable = 0;
    }

    return variable;
}

function row_total(row, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    var val = value;

    var total = val;
    total = replaceWithZero(total);

    if (veg <= 4 && bloom <= 8) {
        return total;
    }

    if (veg >= 5) {
        var dt_pos_veg_4 = table.cell(row, 4).nodes().to$().find('center').html();
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            var dt_pos_flow_3 = table.cell(row, 8).nodes().to$().find('center').html();

            var dt_pos_flow_4 = table.cell(row, 9).nodes().to$().find('center').html();
        } else {
            var dt_pos_flow_3 = table.cell(row, 7).nodes().to$().find('center').html();

            var dt_pos_flow_4 = table.cell(row, 8).nodes().to$().find('center').html();
        }
    }

    if (veg == 5) {
        total = total + (Number(replaceWithZero(dt_pos_veg_4)) * 2);
    }
    if (veg == 6) {
        total = total + (Number(replaceWithZero(dt_pos_veg_4)) * 3);
    }
    if (veg == 7) {
        total = total + (Number(replaceWithZero(dt_pos_veg_4)) * 4);
    }
    //------------------------------------
    if (bloom == 9) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 2);
    }
    if (bloom == 10) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 2) + Number(replaceWithZero(dt_pos_flow_4));
    }
    if (bloom == 11) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 3) + Number(replaceWithZero(dt_pos_flow_4));
    }
    if (bloom == 12) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 3) + (Number(replaceWithZero(dt_pos_flow_4)) * 2);
    }
    if (bloom == 13) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 3) + (Number(replaceWithZero(dt_pos_flow_4)) * 3);
    }
    if (bloom == 14) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 4) + (Number(replaceWithZero(dt_pos_flow_4)) * 3);
    }
    if (bloom == 15) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 5) + (Number(replaceWithZero(dt_pos_flow_4)) * 3);
    }
    if (bloom == 16) {
        total = total + (Number(replaceWithZero(dt_pos_flow_3)) * 5) + (Number(replaceWithZero(dt_pos_flow_4)) * 4);
    }

    return total;
}