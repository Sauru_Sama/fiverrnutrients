function roots_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    try {
        for (var veg_index = 1; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
        //----------------------------------------------------------------------------

        if (bloom <= 8) {
            if (veg <= 4) {
                for (var bloom_index = (Number(veg) + 1); bloom_index < ((Number(veg) + 1) + 7); bloom_index++) {
                    table.cell(rowPosition, bloom_index).nodes().to$().html('<center>' + value + '</center>');
                }
            } else {
                for (var bloom_index = 5; bloom_index < (5 + 6); bloom_index++) {
                    table.cell(rowPosition, bloom_index).nodes().to$().html('<center>' + value + '</center>');
                }
            }
        }

        if (bloom == 9) {
            if (veg <= 4) {
                for (var bloom_index = (Number(veg) + 1); bloom_index < ((Number(veg) + 1) + 6); bloom_index++) {
                    table.cell(rowPosition, bloom_index).nodes().to$().html('<center>' + value + '</center>');
                }
            } else {
                for (var bloom_index = 5; bloom_index < (5 + 7); bloom_index++) {
                    table.cell(rowPosition, bloom_index).nodes().to$().html('<center>' + value + '</center>');
                }
            }
        }

        if (bloom >= 10) {
            if (veg <= 4) {
                for (var bloom_index = (Number(veg) + 1); bloom_index < ((Number(veg) + 1) + 6); bloom_index++) {
                    table.cell(rowPosition, bloom_index).nodes().to$().html('<center>' + value + '</center>');
                }
            } else {
                for (var bloom_index = 5; bloom_index < (5 + 5); bloom_index++) {
                    table.cell(rowPosition, bloom_index).nodes().to$().html('<center>' + value + '</center>');
                }
            }
        }

    } catch (error) {
        console.log("ROOTS");
    }
}

//---------------------------------------------------------

function pre_row_values(rowPosition, veg, value) {
    var table = $('#dataTable').DataTable();

    if (veg >= 3 && veg <= 7) {
        table.cell(rowPosition, 3).nodes().to$().html('<center>' + value + '</center>');
        table.cell(rowPosition, 4).nodes().to$().html('<center>' + value + '</center>');
    }
}

//---------------------------------------------------------

function ultra_row_values(rowPosition, veg, value) {
    var table = $('#dataTable').DataTable();

    if (veg < 5) {
        table.cell(rowPosition, (Number(veg) + 2)).nodes().to$().html('<center>' + value + '</center>');
        table.cell(rowPosition, (Number(veg) + 3)).nodes().to$().html('<center>' + value + '</center>');
    }

    if (veg >= 5) {
        table.cell(rowPosition, 5).nodes().to$().html('<center>' + value + '</center>');
        table.cell(rowPosition, 6).nodes().to$().html('<center>' + value + '</center>');
    }
}

//---------------------------------------------------------

function phat_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (bloom <= 8) {
        if (veg <= 4) {
            table.cell(rowPosition, (Number(veg) + 4)).nodes().to$().html('<center>' + value + '</center>');
            table.cell(rowPosition, (Number(veg) + 5)).nodes().to$().html('<center>' + value + '</center>');
        } else {
            table.cell(rowPosition, (5 + 2)).nodes().to$().html('<center>' + value + '</center>');
            table.cell(rowPosition, (5 + 3)).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            table.cell(rowPosition, (Number(veg) + 4)).nodes().to$().html('<center>' + value + '</center>');
        } else {
            table.cell(rowPosition, (5 + 2)).nodes().to$().html('<center>' + value + '</center>');
        }
    }
}

//---------------------------------------------------------

function ooze_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (bloom == 7) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 6); veg_index < ((Number(veg) + 6) + 3); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 9; veg_index < (9 + 3); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 6); veg_index < ((Number(veg) + 6) + 4); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 9; veg_index < (9 + 4); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 5); veg_index < ((Number(veg) + 5) + 4); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 8; veg_index < (8 + 4); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function final_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (bloom == 7) {
        if (veg <= 4) {
            table.cell(rowPosition, (Number(veg) + 8)).nodes().to$().html('<center>' + value + '</center>');
        } else {
            table.cell(rowPosition, 11).nodes().to$().html('<center>' + value + '</center>');
        }

    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 8); veg_index < ((Number(veg) + 8) + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 11; veg_index < (11 + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 7); veg_index < ((Number(veg) + 7) + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 10; veg_index < (10 + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function sea_minerals_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg <= 4) {
        for (var veg_index = 2; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    } else {
        for (var veg_index = 2; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    //----------------------------------------------------------------------------

    if (bloom >= 6 && bloom <= 7) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 1); veg_index < ((Number(veg) + 1) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 1); veg_index < ((Number(veg) + 1) + 8); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 1); veg_index < ((Number(veg) + 1) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function cal_mag_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg >= 3 && veg <= 4) {
        for (var veg_index = 3; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    if (veg >= 5) {
        for (var veg_index = 3; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    //----------------------------------------------------------------------------

    if (bloom <= 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 1) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 1) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 5); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function orgswtnr_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg >= 1 && veg <= 4) {
        for (var veg_index = 2; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    } else {
        for (var veg_index = 2; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    //---------------------------------------------------------

    if (bloom <= 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 1) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 1) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 5); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function pk_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (bloom >= 7 && bloom <= 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 6); veg_index < ((Number(veg) + 6) + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 9; veg_index < (9 + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 5); veg_index < ((Number(veg) + 5) + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 8; veg_index < (8 + 2); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function silica_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg >= 3 && veg <= 4) {
        for (var veg_index = 3; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    if (veg >= 5) {
        for (var veg_index = 3; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    //----------------------------------------------------------------------------

    if (bloom >= 6 && bloom <= 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + Number(bloom)); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + Number(bloom)); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg)) + Number(bloom)); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function sea_fuel_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (bloom >= 6 && bloom <= 7) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function sea_weed_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg <= 4) {
        for (var veg_index = 2; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    } else {
        for (var veg_index = 2; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }
}

//---------------------------------------------------------

function grow_a_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg <= 4) {
        for (var veg_index = 3; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    } else {
        for (var veg_index = 3; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }
}

//---------------------------------------------------------

function grow_b_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg <= 4) {
        for (var veg_index = 3; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    } else {
        for (var veg_index = 3; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }
}

//---------------------------------------------------------

function flower_a_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (bloom == 6 || bloom == 7) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function flower_b_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (bloom == 6 || bloom == 7) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function euro_a_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg >= 3 && veg <= 4) {
        for (var veg_index = 3; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    if (veg >= 5) {
        for (var veg_index = 3; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    //---------------------------------------------------------

    if (bloom == 6) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 7) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------

function euro_b_row_values(rowPosition, veg, bloom, value) {
    var table = $('#dataTable').DataTable();

    if (veg >= 3 && veg <= 4) {
        for (var veg_index = 3; veg_index < (Number(veg) + 2); veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    if (veg >= 5) {
        for (var veg_index = 3; veg_index < 5; veg_index++) {
            table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
        }
    }

    //---------------------------------------------------------

    if (bloom == 6) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 7) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom == 8) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 7); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

    if (bloom >= 9) {
        if (veg <= 4) {
            for (var veg_index = (Number(veg) + 2); veg_index < ((Number(veg) + 2) + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        } else {
            for (var veg_index = 5; veg_index < (5 + 6); veg_index++) {
                table.cell(rowPosition, veg_index).nodes().to$().html('<center>' + value + '</center>');
            }
        }
    }

}

//---------------------------------------------------------